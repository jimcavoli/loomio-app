FROM cloudron/base:3.0.0

ARG VERSION=2.8.3

# ruby-build dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      autoconf \
      bison \
      build-essential \
      libffi-dev \
      libgdbm-dev \
      libncurses5-dev \
      libreadline-dev \
      libssl-dev \
      libyaml-dev

# Loomio dependencies
RUN apt-get install -y --no-install-recommends \
      imagemagick \
      ffmpeg \
      mupdf \
      libxml2-dev \
      libxslt1-dev

# apt cleanup
RUN apt-get clean &&\
    rm -rf /var/cache/apt /var/lib/apt/lists

# Clone Loomio
RUN mkdir -p /app/code && \
    curl -L https://github.com/loomio/loomio/archive/v${VERSION}.tar.gz | tar -xz --strip-components 1 -C /app/code -f -

# Install the Loomio-specified version of Ruby
RUN echo 'gem: --no-document' >> /usr/local/etc/gemrc && \
    git clone https://github.com/rbenv/ruby-build.git && \
    ruby-build/install.sh && rm -rf ruby-build && \
    (ruby-build $(cat /app/code/.ruby-version) /usr/local)

# Set up build environment
ENV BUNDLE_USER_HOME /app/code/.bundle
ENV BUNDLE_GEMFILE=/app/code/Gemfile
ENV RAILS_ENV production
ENV NODE_ENV production

# Build UI
WORKDIR /app/code/vue
RUN npm install --production=false
RUN npm run build --verbose
RUN rm -rf node_modules

# Build app
WORKDIR /app/code

RUN gem install bundler && \
    bundle config set --local deployment 'true' && \
    bundle config set --local without 'test development' && \
    bundle config set --local jobs $(nproc) && \
    bundle config set --local path /app/code/vendor/gems && \
    bundle install

RUN mkdir -p /app/data && \
    ln -sf /app/pkg/database.yml /app/code/config/database.yml

RUN ln -sf /run/loomio/.env.production /app/code/.env.production && \
    ln -sf /app/data/.env /app/code/.env && \
    ln -sf /app/data/custom /app/code/public/custom && \
    rm -rf /app/code/log && ln -sf /run/loomio/log /app/code/log && \
    rm -rf /app/code/tmp && ln -s /run/loomio/tmp /app/code/tmp && \
    mv -f /app/code/db/schema.rb /app/code/db/schema.orig.rb && \
    ln -s /run/loomio/schema.rb /app/code/db/schema.rb

# Patch selected files to use different reply addresses that work with cloudron inbound mail poller
RUN mkdir -p /app/code/.orig && \
    mv /app/code/app/email_processors/email_params.rb /app/code/.orig/email_params.rb && \
    sed -e "s/email_hash\[:token].to_s.split('\&').each/email_hash[:token].to_s.split('+', 2).last.split('\&').each/" \
      /app/code/.orig/email_params.rb > /app/code/app/email_processors/email_params.rb && \
    mv /app/code/app/helpers/email_helper.rb /app/code/.orig/email_helper.rb && \
    sed -e "s/\[address, ENV\['REPLY_HOSTNAME']].join('@')/[\"#{ENV['CLOUDRON_MAIL_TO'].split('@').first}+#{address}\", ENV['REPLY_HOSTNAME']].join('@')/" \
      /app/code/.orig/email_helper.rb > /app/code/app/helpers/email_helper.rb

RUN chown -R cloudron:cloudron /app/code

# Install channel server
COPY --from=loomio/loomio_channel_server:latest --chown=cloudron:cloudron /app /app/channel_server

# Set up nginx & logging
RUN ln -sf /run/loomio/supervisord.log /var/log/supervisor/supervisord.log && \
    rm /etc/nginx/sites-enabled/* && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stdout /var/log/nginx/error.log && \
    ln -s /etc/nginx/sites-available/loomio /etc/nginx/sites-enabled/loomio

# Copy in cloudron package files; install nginx and supervisor configs
COPY pkg/ /app/pkg/
RUN mv /app/pkg/nginx/readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf && \
    mv /app/pkg/nginx/loomio.conf /etc/nginx/sites-available/loomio && \
    rm -r /app/pkg/nginx && \
    mv /app/pkg/supervisor/* /etc/supervisor/conf.d/ && \
    rm -r /app/pkg/supervisor

WORKDIR /app/code
CMD [ "/app/pkg/start.sh" ]
