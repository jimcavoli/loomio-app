#!/usr/bin/env ruby

# ENV vars used:
# IMAP_POLLER_LOGLEVEL - loger level to emit to STDOUT (default of INFO, options include DEBUG, INFO, WARN, ERROR, FATAL)
# IMAP_POLLER_RETENTION_DAYS - number of days to keep messages on server before purge (default of 1 if not set)
# IMAP_POLLER_LOOMIO_RELAY_URL - full url to the loomio email processor where emails wil be POSTed

require 'json'
require 'logger'
require 'net/http'
require 'net/https'
require 'net/imap'
require 'openssl'

logger = Logger.new($stdout, level: ENV['IMAP_POLLER_LOGLEVEL'] || 'INFO', progname: 'imap_poller/loomio_relay')
retention_cutoff = (Time.now - (60 * 60 * 24 * (ENV['IMAP_POLLER_RETENTION_DAYS'].to_i || 1))).strftime("%d-%b-%Y")

logger.info("Starting #{Process.pid}")
imap = Net::IMAP.new(
  ENV['CLOUDRON_MAIL_IMAP_SERVER'],
  port: ENV['CLOUDRON_MAIL_IMAP_PORT'],
  ssl: { verify_mode: OpenSSL::SSL::VERIFY_NONE }
)
imap.login(ENV['CLOUDRON_MAIL_IMAP_USERNAME'], ENV['CLOUDRON_MAIL_IMAP_PASSWORD'])
imap.select('INBOX')
logger.info("Connected to imap server - #{imap}")

retention_status = [
  "Before: #{imap.search(['BEFORE', retention_cutoff]).length}",
  "Since: #{imap.search(['SINCE', retention_cutoff]).length}",
  "Unseen: #{imap.search(['UNSEEN']).length}",
  "Seen: #{imap.search(['NOT', 'NEW']).length}"
].join(", ")
logger.info("Retention cutoff: #{retention_cutoff} {#{retention_status}}")

logger.debug("Fetching unseen messages")
imap.search(['UNSEEN']).each do |message_id|
  logger.info("Fetching message #{message_id}")
  envelope = imap.fetch(message_id, 'ENVELOPE')[0].attr['ENVELOPE']
  logger.debug("Envelope for message #{message_id}:\n#{envelope}")
  body = imap.fetch(message_id,'BODY[TEXT]')[0].attr['BODY[TEXT]']
  logger.debug("Body for message #{message_id}:\n#{body}")

  message_hash = {
    to: envelope.to.map do |to|
      { name: to.name, address: to.address }
    end,
    from: envelope.from.map do |from|
      { name: from.name, address: from.address }
    end,
    cc: envelope.cc.map do |cc|
      { name: cc.name, address: cc.address }
    end,
    text: body
  }
  logger.debug("Resulting message hash:\n#{message_hash}")

  begin
    logger.info("Sending message #{message_id} to loomio")
    uri = URI(ENV['IMAP_POLLER_LOOMIO_RELAY_URL'])

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER

    req =  Net::HTTP::Post.new(uri)
    req.add_field "Content-Type", "application/json; charset=utf-8"
    req.body = JSON.dump({ mailinMsg: message_hash })

    res = http.request(req)
    logger.debug("Response #{res.code}: #{res.body}")

    logger.info("Marking message #{message_id} as seen")
    imap.store(message_id, '+FLAGS', [:Seen])
  rescue StandardError => e
    logger.error("Failed to transmit message #{message_id} to loomio: #{e.class} - #{e.message}")
  end
end

logger.debug('Searching fro old messages')
old_messages = imap.search(['BEFORE', retention_cutoff])
logger.info("Marking #{old_messages.size} messages from before #{retention_cutoff} for deletion")
old_messages.each do |message_id|
  logger.debug("Setting deleted flag on #{message_id}")
  imap.store(message_id, '+FLAGS', [:Deleted])
end

logger.info('Expunging old messages marked for deletion')
imap.expunge

logger.info("Finished #{Process.pid}")
