#!/bin/bash

set -eu

cd /app/code

echo "==> Creating directories"
mkdir -p /run/loomio/log \
         /run/loomio/tmp \
         /run/nginx/cache \
         /app/data/custom

if [ ! -f /app/data/.env ]; then
    echo "==> Generating initial environment variables"
    echo "SECRET_KEY_BASE=$(bundle exec rails secret)" > /app/data/.env

    echo "==> Copying default environment variables"
    cat /app/pkg/env >> /app/data/.env

    echo "==> Performing initial database setup"
    set -o allexport
    source /app/data/.env
    cp -f /app/code/db/schema.orig.rb /run/loomio/schema.rb
    bundle exec rails db:schema:load
    rm -f /run/loomio/schema.rb
fi

echo "==> Configuring Loomio"
{
  echo "CANONICAL_HOST=${CLOUDRON_APP_DOMAIN}"
  echo "CHANNELS_URI=wss://${CLOUDRON_APP_DOMAIN}"
  echo "REDIS_URL=${CLOUDRON_REDIS_URL}"
  echo "SMTP_AUTH=login"
  echo "SMTP_DOMAIN=${CLOUDRON_MAIL_DOMAIN}"
  echo "SMTP_SERVER=${CLOUDRON_MAIL_SMTP_SERVER}"
  echo "SMTP_PORT=${CLOUDRON_MAIL_SMTPS_PORT}"
  echo "SMTP_USERNAME=${CLOUDRON_MAIL_SMTP_USERNAME}"
  echo "SMTP_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}"
  echo "SMTP_USE_SSL=1"
  echo "HELPER_BOT_EMAIL=${CLOUDRON_MAIL_FROM}"
  echo "NOTIFICATIONS_EMAIL_ADDRESS=${CLOUDRON_MAIL_FROM}"
  echo "REPLY_HOSTNAME=${CLOUDRON_MAIL_DOMAIN}"
  echo "PUMA_WORKERS=$(nproc)"
  echo "MIN_THREADS=12"
  echo "MAX_THREADS=12"
  echo "FORCE_SSL=1"
  echo "USE_RACK_ATTACK=1"
  echo "IMAP_POLLER_LOOMIO_RELAY_URL=${CLOUDRON_APP_ORIGIN}/email_processor/"
} > /run/loomio/.env.production

set -o allexport
source /app/data/.env
source /run/loomio/.env.production

echo "==> Migrating Database"
bundle exec rails db:migrate

echo "==> Changing permissions"
chown -R cloudron:cloudron /run/loomio /app/data

echo "==> Starting app"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i loomio
